# Use the PHP image with Apache
FROM php:8.1.6-apache

# Update package list and install necessary dependencies
RUN apt-get update && apt-get install -y \
    libpng-dev \
    libjpeg-dev \
    libfreetype6-dev \
    libzip-dev \
    zip \
    unzip \
    software-properties-common \
    gnupg \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install gd pdo pdo_mysql mysqli zip \
    && curl -fsSL https://deb.nodesource.com/setup_14.x | bash - \
    && apt-get install -y nodejs

# Enable Apache modules
RUN a2enmod rewrite
# Enable mod_headers module
RUN a2enmod headers
# Copy the Apache configuration file
COPY apache-config.conf /etc/apache2/sites-available/000-default.conf

# Set working directory
WORKDIR /var/www/html

# Copy your Vue.js projects into the container
COPY host-side /var/www/html/host-side
COPY remote-a /var/www/html/remote-a

# Expose ports
EXPOSE 80
EXPOSE 443