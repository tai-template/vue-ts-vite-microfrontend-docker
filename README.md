After downloading, do the following steps:
# remote-a
```
npm i
npm run build
```
# host-side
```
npm i
npm run build
```
# host configuration 
I use window so I will open `C:\Windows\System32\drivers\etc\hosts` file and paste it:
```
127.0.0.1 host-side.local
127.0.0.1 remote-a.local
```
# docker
```
docker compose build
docker compose up -d
```
Visit `host-side.local:8080` and `remote-a.local:8080` in the browser to explore

Read more: https://lazycodet.com/3/read/447