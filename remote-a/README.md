# About
This is a template project `Vue3.4 + Typescript + Vite` and some other libraries like:
+ Bootstrap 5
+ Axios
# After download
```
cd <path-project>
npm install
npm run dev
```
# Feature
## 1. Detection click the back/forward button
This project is built to detect when users click the back/forward button. You can check this event like this:
```
import MyApp from './MyApp';
if(MyApp.isFromBackButton)
    ...
```
Read more: https://lazycodet.com/3/read/443
